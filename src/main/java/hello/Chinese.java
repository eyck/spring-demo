package hello;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.*;

/**
 * Created by FUN on 2017/7/26.
 */
@Component
@Lazy(true)
public class Chinese implements Person, ApplicationContextAware, InitializingBean{

    private ApplicationContext applicationContext;

    private String name;
    @Resource(name = "stoneAxe")
    private Axe axe;
    private List<String> schools;
    private Map scorse;
    private Map<String, Axe> phaseAxes;
    private Properties health;
    private Set axes;
    private String[] books;

    public Chinese() {
        System.out.println("Chinese()");
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

   // @Resource(name = "stoneAxe")
    public void setAxe(Axe axe) {
        this.axe = axe;
    }

    public void useAxe() {
        System.out.println("I am " + getName());
        axe.chop();
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void sayHi(String name) {
        System.out.println(applicationContext.getMessage("hello", new String[]{name}, Locale.getDefault(Locale.Category.FORMAT)));
    }

    public void setSchools(List<String> schools) {
        this.schools = schools;
    }

    public void setScorse(Map scorse) {
        this.scorse = scorse;
    }

    public void setPhaseAxes(Map<String, Axe> phaseAxes) {
        this.phaseAxes = phaseAxes;
    }

    public void setHealth(Properties health) {
        this.health = health;
    }

    public void setAxes(Set axes) {
        this.axes = axes;
    }

    public void setBooks(String[] books) {
        this.books = books;
    }

    public void test() {
        System.out.println(schools);
        System.out.println(scorse);
        System.out.println(phaseAxes);
        System.out.println(health);
        System.out.println(axes);
        System.out.println(java.util.Arrays.toString(books));
    }

    @PostConstruct
    public void init() {
        System.out.println("Runnint Chinese init()");
    }

    public void afterPropertiesSet() throws Exception {
        System.out.println("Runnint Chinese afterPropertiesSet()");
    }

    @PreDestroy
    public void close() {
        System.out.println("Runnint Chinese close()");
    }
}
