package hello;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by FUN on 2017/7/26.
 */
@Component
@Scope("prototype")
public class StoneAxe implements Axe {
    public void chop() {
        System.out.println("Run StoneAxe chop()");
    }
}
