package hello;

/**
 * Created by FUN on 2017/7/26.
 */
public interface Person {
    public void useAxe();

    public void sayHi(String name);

    public void test();
}
