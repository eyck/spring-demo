package aspectj_test;

/**
 * Created by FUN on 2017/7/31.
 */
public class Hello {
    public void foo() {
        System.out.println("Running Hello foo().");
    }

    public int addUesr(String name, String pass) {
        System.out.println("Running Hello addUesr(" + name + ", " + pass + ").");
        return 20;
    }
}
