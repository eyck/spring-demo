package synamic_proxy_test2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by FUN on 2017/7/31.
 */
public class ProxyTest {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        Hello hello = (Hello) ctx.getBean("helloImpl");
        hello.foo();
        hello.addUser("eyck", "123456");
    }
}
