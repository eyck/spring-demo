package synamic_proxy_test2;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * Created by FUN on 2017/7/31.
 */
@Component
@Aspect
public class LogAspect {

    @Before("execution(* *.*.*(..))")
    public void authority() {
        System.out.println("Running LogAspect authority().");
    }
}
