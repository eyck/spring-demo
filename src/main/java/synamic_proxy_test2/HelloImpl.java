package synamic_proxy_test2;

import org.springframework.stereotype.Component;

/**
 * Created by FUN on 2017/7/31.
 */
@Component
public class HelloImpl implements Hello {
    public void foo() {
        System.out.println("Running HelloImpl foo().");
    }

    public void addUser(String name, String pass) {
        System.out.println("Running HelloImpl addUser(" + name + ", " + pass + ").");
    }
}
