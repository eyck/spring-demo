package synamic_proxy_test2;

/**
 * Created by FUN on 2017/7/31.
 */
public interface Hello {
    void foo();

    void addUser(String name, String pass);
}
