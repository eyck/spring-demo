package ehcache_test2;

/**
 * Created by FUN on 2017/8/2.
 */
public interface MovieDao {
    Movie findByDirector(String name);
}
