package ehcache_test2;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

/**
 * Created by FUN on 2017/8/2.
 */
@Repository("movieDao")
public class MovieDaoImpl implements MovieDao {

    @Cacheable("users")
    public Movie findByDirector(String name) {
        slowQuery(2000L);
        System.out.println("findByDirector is running...");
        return new Movie(1, "Forrest Gump", "Robert Zemeckis");
    }

    private void slowQuery(long seconds) {
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
