package ehcache_test2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by FUN on 2017/8/2.
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        MovieDao movieDao = (MovieDao) context.getBean("movieDao");
        movieDao.findByDirector("dummy");
        movieDao.findByDirector("dummy");
        movieDao.findByDirector("dummy");
//        logger.info("The time is now {}", new SimpleDateFormat("HH:mm:ss").format(new Date()));
//        logger.debug("Result: {}", movieDao.findByDirector("dummy"));
//        logger.debug("Result: {}", movieDao.findByDirector("dummy"));
//        logger.debug("Result: {}", movieDao.findByDirector("dummy"));
        ((ConfigurableApplicationContext)context).close();
    }
}
