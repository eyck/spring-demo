package synamic_proxy_test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

//
//import java.lang.reflect.InvocationHandler;
//import java.lang.reflect.Method;
//import java.lang.reflect.Proxy;
//
///**
// * Created by FUN on 2017/7/31.
// */
//interface Person {
//    void walk();
//    void sayHello(String name);
//}
//
//class MyInvokationHandler implements InvocationHandler {
//
//    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        System.out.println("proxy: " + proxy.getClass().getName());
//        System.out.println("Running: " + method);
//        if(args != null) {
//            System.out.println("args: ");
//            for(Object obj: args) {
//                System.out.println(obj);
//            }
//        } else {
//            System.out.println("No args of this method.");
//        }
//        return null;
//    }
//}
public class ProxyTest {
    public static void main(String[] args) {
//        InvocationHandler handler = new MyInvokationHandler();
//        Person p = (Person) Proxy.newProxyInstance(Person.class.getClassLoader(), new Class[]{Person.class}, handler);
//        p.walk();
//        p.sayHello("Eyck");
        MyInvocationHandler handler = new MyInvocationHandler();
        handler.setTarget(new GunDog());
        Dog dog = (Dog) Proxy.newProxyInstance(Dog.class.getClassLoader(), new Class[]{Dog.class}, handler);
        dog.info();
        dog.run();
    }
}

