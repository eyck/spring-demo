package synamic_proxy_test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by FUN on 2017/7/31.
 */
public class MyInvocationHandler implements InvocationHandler{
    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        DogUtil dogUtil = new DogUtil();
        dogUtil.method1();
        method.invoke(target, args);
        dogUtil.method2();
        return null;
    }
}
