package synamic_proxy_test;

/**
 * Created by FUN on 2017/7/31.
 */
public interface Dog {
    void info();
    void run();
}
