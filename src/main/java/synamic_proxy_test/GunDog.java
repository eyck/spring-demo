package synamic_proxy_test;

/**
 * Created by FUN on 2017/7/31.
 */
public class GunDog implements Dog {
    public void info() {
        System.out.println("Running GunDog info().");
    }

    public void run() {
        System.out.println("Running GunDog run().");
    }
}
