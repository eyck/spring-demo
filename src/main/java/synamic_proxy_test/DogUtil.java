package synamic_proxy_test;

/**
 * Created by FUN on 2017/7/31.
 */
public class DogUtil {
    public void method1() {
        System.out.println("Running GunDog mathod1().");
    }
    public void method2() {
        System.out.println("Running GunDog mathod2().");
    }
}
