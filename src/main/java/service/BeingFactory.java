package service;

/**
 * Created by FUN on 2017/7/26.
 */
public class BeingFactory {
    public static Being getBeing(String arg) {
        if (arg.equalsIgnoreCase("dog")) {
            return new Dog();
        } else {
            return new Cat();
        }
    }
}
