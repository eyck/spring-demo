package service;

import service.Being;

/**
 * Created by FUN on 2017/7/26.
 */
public class Cat implements Being {
    private String msg;
    public void testBeing() {
        System.out.println(msg + "Running Cat testBeing");
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
