package service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by FUN on 2017/7/26.
 */
public class BeingTest {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        Being b1 = ctx.getBean("dog", Being.class);
        b1.testBeing();
        Being b2 = ctx.getBean("cat", Being.class);
        b2.testBeing();
    }
}
