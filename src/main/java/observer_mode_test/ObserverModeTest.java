package observer_mode_test;

/**
 * Created by FUN on 2017/8/4.
 */
public class ObserverModeTest {
    public static void main(String[] args) {
        Product p = new Product("TV", 176);
        NameObserver no = new NameObserver();
        PriceObserver po = new PriceObserver();
        p.registObserver(no);
        p.registObserver(po);
        p.setName("Computer");
        p.setPrice(2000);
    }
}
