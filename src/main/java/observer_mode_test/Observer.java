package observer_mode_test;

/**
 * Created by FUN on 2017/8/4.
 */
public interface Observer {
    void update(Observable observable, Object obj);
}
