package observer_mode_test;

/**
 * Created by FUN on 2017/8/4.
 */
public class Product extends Observable {
    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        notifyObservers(name);
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        notifyObservers(name);
        this.price = price;
    }

    public Product(String name, double price) {

        this.name = name;
        this.price = price;
    }

    public Product() {

    }
}
