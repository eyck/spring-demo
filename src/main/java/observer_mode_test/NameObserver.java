package observer_mode_test;

import javax.swing.*;

/**
 * Created by FUN on 2017/8/4.
 */
public class NameObserver implements Observer {
    public void update(Observable observable, Object obj) {
        if(obj instanceof String) {
            String name = (String) obj;
            JFrame f = new JFrame("Observer");
            JLabel l = new JLabel("name chagned to: " + name);
            f.add(l);
            f.pack();
            f.setVisible(true);
            System.out.println("Name Observer:" + observable + ", name changed to: " + name);
        }
    }
}
