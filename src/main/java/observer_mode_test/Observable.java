package observer_mode_test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FUN on 2017/8/4.
 */
public abstract class Observable {
    List<Observer> observers = new ArrayList<Observer>();
    public void registObserver(Observer observer) {
        observers.add(observer);
    }
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }
    public void notifyObservers(Object value) {
        for (Observer observer: observers) {
            observer.update(this, value);
        }
    }
}
