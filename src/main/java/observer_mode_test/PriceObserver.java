package observer_mode_test;

/**
 * Created by FUN on 2017/8/4.
 */
public class PriceObserver implements Observer {
    public void update(Observable observable, Object obj) {
        if(obj instanceof Double) {
            System.out.println("Price Observer: " + observable + ", price changed to: " + obj);
        }
    }
}
