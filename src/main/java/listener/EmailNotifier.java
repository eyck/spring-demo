package listener;

import event.EmailEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * Created by FUN on 2017/7/26.
 */
public class EmailNotifier implements ApplicationListener{

    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        if(applicationEvent instanceof EmailEvent) {
            EmailEvent emailEvent = (EmailEvent) applicationEvent;
            System.out.println("address: " + emailEvent.getAddress());
            System.out.println("text: " + emailEvent.getText());
        } else {
            System.out.println("other: " + applicationEvent);
        }
    }
}
