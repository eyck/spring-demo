package config;

import hello.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by FUN on 2017/7/26.
 */
@Configuration
public class AppConfig {
    @Value("Woosley")
    String personName;

    @Bean(name = "chinese")
    public Person person() {
        Chinese chinese = new Chinese();
        chinese.setAxe(stoneAxe());
        chinese.setName(personName);
        return chinese;
    }

    @Bean(name = "stoneAxe")
    public Axe stoneAxe() {
        return new StoneAxe();
    }

    @Bean(name = "steelAxe")
    public Axe steelAxe() {
        return new SteelAxe();
    }
}
