package ehcache_test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by FUN on 2017/8/2.
 */
public class EhcacheTest {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        UserService userService = (UserService) ctx.getBean("userService", UserService.class);
        userService.getUserByName("eyck");
        userService.getUserByName("eyck");
        userService.getUserByName("eyck");
        ((ConfigurableApplicationContext)ctx).close();
    }
}
