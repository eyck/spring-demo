package ehcache_test;

/**
 * Created by FUN on 2017/8/2.
 */
public interface UserService {
    User getUserByName(String name);

    User getUserByNameAndPass(String name);

}
