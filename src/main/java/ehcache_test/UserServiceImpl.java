package ehcache_test;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by FUN on 2017/8/2.
 */

@Service("userService")
public class UserServiceImpl implements UserService {

    @Cacheable("users")
    public User getUserByName(String name) {
        System.out.println("Running UserServiceImpl getUserByName(" + name + ")");
        return new User(name);
    }

    public User getUserByNameAndPass(String name) {
        System.out.println("Running UserServiceImpl getUserByNameAndPass(" + name + ")");
        return new User(name);
    }

}
