package stragegy_mode;

/**
 * Created by FUN on 2017/7/29.
 */
public class DiscountContext {
    private DiscountStrategy discountStrategy;

    public DiscountContext(DiscountStrategy discountStrategy) {
        this.discountStrategy = discountStrategy;
    }

    public double getDiscountPrice(double price) {
        if ( discountStrategy == null ){
            discountStrategy = new OldDiscount();
        } else {
            discountStrategy = new VipDiscount();
        }
        return discountStrategy.getDiscount(price);
    }
}
