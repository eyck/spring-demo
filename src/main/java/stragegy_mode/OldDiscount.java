package stragegy_mode;

/**
 * Created by FUN on 2017/7/29.
 */
public class OldDiscount implements DiscountStrategy {
    public double getDiscount(double originPrice) {
        System.out.println("Using Old Discount...");
        return originPrice * 0.7;
    }
}
