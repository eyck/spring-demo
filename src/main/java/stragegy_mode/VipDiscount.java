package stragegy_mode;

/**
 * Created by FUN on 2017/7/29.
 */
public class VipDiscount implements DiscountStrategy {
    public double getDiscount(double originPrice) {
        System.out.println("Using Vip Discount...");
        return originPrice * 0.5;
    }
}
