package stragegy_mode;

/**
 * Created by FUN on 2017/7/29.
 */
public interface DiscountStrategy {
    double getDiscount(double originPrice);
}
