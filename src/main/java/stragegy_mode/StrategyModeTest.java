package stragegy_mode;

/**
 * Created by FUN on 2017/7/29.
 */
public class StrategyModeTest {
    public static void main( String[] args ) {
        DiscountContext discountContext = new DiscountContext(null);
        double price1 = 100;
        System.out.println("default discount price: " +  discountContext.getDiscountPrice(price1));

        discountContext = new DiscountContext(new VipDiscount());
        System.out.println("VIP  discount price: " +  discountContext.getDiscountPrice(price1));
    }
}
