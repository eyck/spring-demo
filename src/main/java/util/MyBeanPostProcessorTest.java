package util;

import hello.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by FUN on 2017/7/27.
 */
public class MyBeanPostProcessorTest {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        Person person = ctx.getBean("chinese", Person.class);
        person.useAxe();
    }
}
