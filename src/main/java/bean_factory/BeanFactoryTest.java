package bean_factory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by FUN on 2017/7/27.
 */
public class BeanFactoryTest {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(ctx.getBean("north"));
        System.out.println(ctx.getBean("&north"));
        System.out.println(ctx.getBean("theValue"));
        System.out.println(ctx.getBean("&theValue"));


    }
}
