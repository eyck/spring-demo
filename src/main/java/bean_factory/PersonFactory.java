package bean_factory;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Field;

/**
 * Created by FUN on 2017/7/27.
 */
public class PersonFactory implements FactoryBean{
    private String targetClass;
    private String targetField;
    public Object getObject() throws Exception {
        Class<?> clazz =Class.forName(targetClass);
        Field field = clazz.getField(targetField);
        return field.get(null);
    }

    public Class<?> getObjectType() {
        return Object.class;
    }

    public boolean isSingleton() {
        return false;
    }

    public void setTargetClass(String targetClass) {
        this.targetClass = targetClass;
    }

    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }
}
