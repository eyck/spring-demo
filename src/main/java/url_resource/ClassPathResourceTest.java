package url_resource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileSystemUtils;

/**
 * Created by FUN on 2017/7/31.
 */
public class ClassPathResourceTest {
    public static void main(String[] args) {
        ClassPathResource cr = new ClassPathResource("beans.xml");
        System.out.println(cr.getFilename());
        System.out.println(cr.getDescription());
        FileSystemResource fsr = new FileSystemResource("beans.xml");
        System.out.println(fsr.getFilename());
        System.out.println(fsr.getDescription());
        ApplicationContext ctx = new FileSystemXmlApplicationContext("beans.xml");
        Resource r = ctx.getResource("beans.xml");
        System.out.println(r.getFilename());
        System.out.println(r.getDescription());

    }

}
