package url_resource;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.core.io.UrlResource;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by FUN on 2017/7/29.
 */
public class UrlResourceTest {
    public static void main(String[] args) throws IOException, DocumentException {
        UrlResource urlResrouce = new UrlResource("file:book.xml");
        System.out.println(urlResrouce.getFilename());
        System.out.println(urlResrouce.getDescription());
        SAXReader reader = new SAXReader();
        Document document = reader.read(urlResrouce.getFile());
        Element element = document.getRootElement();
        List list = element.elements();
        for (Iterator<Element> it = list.iterator(); it.hasNext(); ) {
            Element element1 = it.next();
            System.out.println(element1.getText());
        }
    }
}
