import event.EmailEvent;
import hello.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Created by FUN on 2017/7/26.
 */
public class BeanTest {
    public static void main(String[] args) throws Exception {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(Arrays.toString(ctx.getBeanDefinitionNames()));
        Person person = ctx.getBean("chinese", Person.class);
        person.useAxe();
        ctx.registerShutdownHook();
    }
}
